#2.2.2 邀請碼

* 當玩家任選一種登入方式登入時，系統將會自動生成邀請碼(每種登入方式獨立計算)
* 點選分享邀請碼，將會如同 ** [2.0a FB分享](https://rctchen.gitlab.io/livebingoapp/2.0a/) ** 頁面彈出FB分享式窗(此功能僅限FB登入)

![2.2.2](./2.2.2.jpg)

參考圖
---
![2.2.2_2](./2.2.2_2.jpg)