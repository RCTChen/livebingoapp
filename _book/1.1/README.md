#1.1 訪客登入

當以訪客登入，將會直接進入遊戲大廳，但左上角的原** [個人資訊](https://rctchen.gitlab.io/livebingoapp/2.2/) ** 按鈕，將會保持在 ** 登入 ** 按鈕狀態，
當玩家再次選擇登入，將會如同** [1.0a 登入視窗](https://rctchen.gitlab.io/livebingoapp/1.0a/) ** 畫面，彈出登入視窗(畫面不動)

![1.1](./1.1.jpg)