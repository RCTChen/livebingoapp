#UI Flow

##APP中頁面間的操作動線

* 符號標示

> 2.0 大廳 (頁面1)
>> 2.0a FB分享 (在頁面1中，** 不轉入新頁面 **，彈出額外視窗)
>>> 2.2 個人資訊 (由頁面1進入，** 轉入新頁面2** )
>>>> 2.2.1 好友 (由頁面2進入，** 轉入新頁面3 **)
>>>>> 2.2.1b 送禮 (在頁面3中，** 不轉入新頁面 **，彈出額外視窗)

![UI Flow](./UI Flow_20190510.jpg)

** 從登入大廳開始，任何一個頁面都應該要可以導向儲值頁面。 **

