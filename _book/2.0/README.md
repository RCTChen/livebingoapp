#2.0 大廳畫面

* ** [FB分享](https://rctchen.gitlab.io/livebingoapp/2.0a/) ** 只有以FB登入方式才會顯示，否則隱藏
* ** 遊戲分類 ** 、** 遊戲圖卡 ** 、** 熱門遊戲 ** 將可以依照目前現有產品進行調整
* 畫面的滑動效果可參照 ** [其它畫面效果](https://rctchen.gitlab.io/livebingoapp/OtherEffect/) ** 

![2.0](./2.0.jpg)

參考圖
---
![2.0_2](./2.0_2.jpg)